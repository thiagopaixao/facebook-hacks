import requests
import json


page_id = ""
post_id = ""
access_token = ""

template = "https://graph.facebook.com/v2.8/%s/posts?access_token=%s"
url = template % (page_id, access_token,)

likes = []
posts = []
n = 0

while(1):

	r = requests.get(url)
	result = r.json()

	page_posts = result

	print(page_posts)

	posts += page_posts['data']

	if('paging' in likes):
		if ('next' not in page_posts['paging']):
			break
		url = page_posts['paging']['next']

	else: break

for post in posts:
	
	post_id = post['id']

	print(post_id)
	template = "https://graph.facebook.com/v2.8/%s/likes?access_token=%s"
	url = template % (post_id, access_token,)

	while 1:
		r = requests.get(url)
		result = r.json()

		page_likes = result
		likes += page_likes['data']
		if('paging' in likes):
			if ('next' not in page_likes['paging']):
				break
			url = page_likes['paging']['next']
		else: break


	fp = open("%s_likes.csv" % (post_id), "w")

	fp.write("Name,Profile Link\n")
	for liker in likes:
		fp.write(liker['name'] + "," + "https://facebook.com/" + liker['id'] + "\n")
	fp.close()

